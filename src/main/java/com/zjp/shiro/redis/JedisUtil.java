package com.zjp.shiro.redis;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

@Component
public class JedisUtil {
	@Value("${redis.cache.nodes}")
	private String nodes;
	
	@Bean
	public JedisCluster getJedisCluster(){
		GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
		String[] serverArray = nodes.split(";");
		Set<HostAndPort> nodeSet = new HashSet<HostAndPort>();
		for(String server :serverArray ){
			String[] ipAndPort = server.split(":");
			HostAndPort hostAndPort = new HostAndPort(ipAndPort[0],Integer.parseInt(ipAndPort[1]));
			nodeSet.add(hostAndPort);
		}
		return new JedisCluster(nodeSet, genericObjectPoolConfig);
	}
	
}

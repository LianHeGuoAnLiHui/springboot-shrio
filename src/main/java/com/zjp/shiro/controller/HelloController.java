package com.zjp.shiro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import redis.clients.jedis.JedisCluster;

@RestController
public class HelloController {

	@Value("${server.name}")
	private String value;
	
	@Autowired
	private JedisCluster jedisCluster;
	
	@RequestMapping("/hello")
	public String index(){
		jedisCluster.set("name", "zhangjiapeng");
		return "hello "+value+" world -" + jedisCluster.get("name");
	}
	
	@RequestMapping("/hello2")
	public String hello() throws Exception {
	    throw new Exception("发生错误");
	}
}

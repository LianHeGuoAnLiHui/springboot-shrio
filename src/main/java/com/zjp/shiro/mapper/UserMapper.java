package com.zjp.shiro.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
	
	@Insert("insert into user(name,age) values (#{name},#{age})")
	int insert(@Param("name") String name,@Param("age") int age);
}

package com.zjp.shiro.mapper2;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface UserMapper2 {
	
	@Insert("insert into user(name,age) values (#{name},#{age})")
	int insert(@Param("name") String name,@Param("age") int age);
}
